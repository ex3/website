<?php

/**
 * Class FormValidator
 */
class FormValidator
{

    public static function validate(array $formData, array $requiredFields = array(),
                                    array $requiredCheckboxes = array(), array $minLengthFields = array(),
                                    array $patternFields = array())
    {
        $errors = array();

        foreach ($requiredFields as $field) {
            if (isset($errors[$field])) continue;
            if (!isset($formData[$field]) || strlen(trim($formData[$field])) == 0) {
                $errors[$field] = ' fehlt.';
                continue;
            }
        }
        foreach ($requiredCheckboxes as $field) {
            if (isset($errors[$field])) continue;
            if (!isset($formData[$field]) || !$formData[$field]) {
                $errors[$field] = ' fehlt.';
                continue;
            }
        }
        foreach ($minLengthFields as $field => $minLength) {
            if (isset($errors[$field])) continue;
            if (isset($formData[$field]) && strlen(trim($formData[$field])) < $minLength) {
                $errors[$field] = ' zu kurz.';
                continue;
            }
        }
        foreach ($patternFields as $field => $pattern) {
            if (isset($errors[$field])) continue;
            if (isset($formData[$field]) && !preg_match($pattern, trim($formData[$field]))) {
                $errors[$field] = ' ungültig.';
                continue;
            }
        }

        return $errors;
    }
}