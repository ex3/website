<?php

class EmailAdapter {

	protected $_type;
	protected $_to;
	protected $_from;
	protected $_data;


	public function __construct($type = null) {
		if ( is_null($type) || empty($type) ) {
			throw new Exception('Bitte einen Email-Typ definieren.');
		}

		$type = $this->dashesToCamelCase($type);

		if ( !method_exists($this, $type) ) {
			throw new Exception('Der uebergebene Email-Typ ist ungueltig.');
		}

		$this->_type = $type;

	}

	public function to($email = '') {
		if ( empty($email) ) {
			throw new Exception('Bitte eine Empfaenger-Email definieren.');
		}
		$this->_to = $email;
		return $this;
	}

	public function from($email = '') {
		if ( empty($email) ) {
			throw new Exception('Bitte eine Absender-Email definieren.');
		}
		$this->_from = $email;
		return $this;
	}

	public function data(array $arr = array()) {
		$this->_data = $arr;
		return $this;
	}

	/**
	 * Ruft die entsprechende Methode auf.
	 */
	public function send() {
		return $this->{$this->_type}();
	}

	/**
	 * Sendet eine Kontaktanfrage an die E-Mail vom Kunden
	 */
	protected function contactRequest() {


		$message = <<<MSG
<h3>Es wurde eine Kontaktanfrage von accom.de gesendet</h3>
<p>Name: [name]</p>
<p>Telefon: [phone]</p>
<p>E-Mail Adresse: [email]</p>
<p>Der Interessent hat folgende Nachricht hinterlassen:</p>

[message]

<p>Diese E-Mail wurde automatisch vom Kontaktformular der Accom-Website versendet</p>
MSG;

		$mailer = wireMail();
		return $mailer
			->from($this->_from)
			->to($this->_to)
			->subject(mb_convert_encoding('Kontaktanfrage von accom.de', 'ISO-8859-1', 'UTF-8'))
			->bodyHTML( $this->replaceTokens($message, $this->_data)  )
			->send() > 0;

	}


	/**
	 * Dash-Notation in Camel-Case-Notation konvertieren
	 *
	 * @param  string $str
	 * @return string
	 */
	protected function dashesToCamelCase($str = '') {
		$arr = array_map(function ($item) {
			return ucfirst($item);
		}, explode('_', $str));
		return lcfirst(implode('', $arr));
	}

	/**
	 * Platzhalter im  `$str` ersetzen
	 *
	 * @param string $str
	 * @param array  $tokens
	 */
	protected function replaceTokens($str, array $tokens) {
		foreach ($tokens as $token => $value) {
			$str = str_replace('[' . $token . ']', $value, $str);
		}
		return $str;
	}



}
