<?php

class Util {

	/**
	 * Nimmt einen assoziativen Array und bereinigt die Values.
	 * Gut fuer POST- und GET-Input.
	 * 
	 * @param  array  $data
	 * @return array
	 */
	public static function sanitize(array $data = array()) {

		$result = array();
		$sanitizer = wire('sanitizer');

		foreach ( $data as $key => $val ) {
			$result[$key] = $sanitizer->text($val);
		}

		return $result;

	}

	/**
	 * Begrenzt einen Text auf eine Wortzahl und ntfernt alle Tags.
	 * 
	 * @param  string  $text
	 * @param  integer $length
	 * @param  string  $suffix
	 * @return string
	 */
	public static function truncate($text = '', $length = 20, $suffix = '&hellip;') {

		$text = strip_tags(trim($text));

		if ( str_word_count($text, 0) > $length ) {
			$words = str_word_count($text, 2);
			$pos = array_keys($words);
			$text = substr($text, 0, $pos[$length]) . ' ' . $suffix;
		}

		return $text;

	}

}