<?php

	class FooterChunk extends \nw\DataProviders\ChunkDataProvider {

		public function setContext( array $context ) {

			$pages = wire('pages');
			$this->context = isset($context[0]) ? $context[0] : array();
			$this->footerMetas = $pages->get('/meta/')->children();
			$this->footerHome = $pages->get('/');



		}

		public function populate() {

		}


	}
