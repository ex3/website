<?php
require_once 'helpers/FormValidator.php';
require_once 'helpers/EmailAdapter.php';
require_once 'helpers/Util.php';

class ContactChunk extends \nw\DataProviders\ChunkDataProvider {

    public $response =  array(
        'fields' => array(),
        'success' => false,
        'submitted' => false,
        'message' => null,
        'errors' => null
    );

    public function setContext( array $context ) {

        $pages = wire('pages');
        $this->context = isset($context[0]) ? $context[0] : array();


    }

    public function populate() {
        if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
            $this->formResponse = $this->getReponse( Util::sanitize($_POST) );
        }
    }

    /**
     * Verarbeitet den POST-Request
     *
     * @param  array  $post POST-Daten
     * @return array        Response-Objekt
     */
    protected function getReponse(array $post = array()) {

        $response =  array(
            'fields' => array(),
            'success' => false,
            'submitted' => true,
            'message' => null,
            'errors' => null
        );

        do {

            $requiredFields = array('name', 'email', 'phone', 'message');

            $requiredCheckboxes = array();
            $minLengthFields = array(
                'name' => 3,
                'email' => 5,
                'phone' => 5,
                'message' => 5,
            );
            $patternFields = array(
                'email' => '/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/'
            );

            $response['errors'] = FormValidator::validate($post, $requiredFields, $requiredCheckboxes, $minLengthFields, $patternFields);

            if ( !empty($response['errors']) ) {
                break;
            }

            /**
             * Mails versenden
             */
            if ( !$this->sendMails($post) ) {
                $response['errors'][] = 'Das Senden der E-Mail ist fehlgeschlagen.';
                break;
            }

            $response['success'] = true;

        } while (false);

        return $response;

    }

    /**
     * Schickt Mails an den in der Kontakt-Seite angegebenen Empfänger
     *
     * @param  array   $post POST-Daten
     * @return boolean
     */
    protected function sendMails(array $post) {

        $contactRequest = new EmailAdapter('contact_request');
        $maSent = $contactRequest
            ->to( "entwicklung@neuwaerts.de" )
            ->from( $post['email'] )
            ->data($post)
            ->send();

        return $maSent;

    }


}
