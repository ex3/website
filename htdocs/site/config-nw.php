<?php
/**
 * Basic Config from neuwaerts
 * Usage: require this file in config.php to use it (realy cool ;])
 * Example: require_once('config-nw.php');
 */


$config->twig = true; //force twig
/**
 *  Extension things:
 */
if( $config->twig || is_file(dirname(__FILE__) . "/templates/admin.twig")) {
    $config->templateExtension = 'twig';
}



/**
 *    DB Setup
 */
switch($_SERVER['SERVER_NAME']) {


    /************************************/
    /*        L O C A L H O S T         */
    /************************************/
    case "localhost" :

        /**
         * Installer: Database Configuration
         *
         */
        $config->dbHost = 'localhost';
        $config->dbName = 'ex-3';
        $config->dbUser = 'ex-3';
        $config->dbPass = 'vjCrNrWq4taY7m55';
        $config->dbPort = '3306';

        $config->urls->httpHost = "http://localhost";

        $config->hostname = 'http://localhost';

        $config->debug = true;

        break;

    /************************************/
    /*              D E V               */
    /************************************/
    case "nw.neuwaerts.webseiten.cc/" :

        /**
         * Installer: Database Configuration
         *
         */
        $config->dbHost = 'mysql5.neuwaerts.webseiten.cc';
        $config->dbName = '';
        $config->dbUser = '';
        $config->dbPass = '';
        $config->dbPort = '3306';

        $config->urls->httpHost = "http://nw.neuwaerts.webseiten.cc/";
        $config->hostname = 'http://nw.neuwaerts.webseiten.cc/';

        $config->debug = false;

        break;

    /************************************/
    /*            L I V E               */
    /************************************/
    default :

        /**
         * Installer: Database Configuration
         *
         */
        $config->dbHost = 'mysql5.neuwaerts.webseiten.cc';
        $config->dbName = '';
        $config->dbUser = '';
        $config->dbPass = '';
        $config->dbPort = '3306';

        $config->urls->httpHost = "http://nw.neuwaerts.webseiten.cc/";
        $config->hostname = 'http://nw.neuwaerts.webseiten.cc/';

        $config->debug = false;

        break;
}
