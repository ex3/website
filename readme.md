# EX3


## Description:

Author: Pascal Franzke & Domenic Steffmann

Repos: https://bitbucket.org/ex3/website

This is EX3

Bower Libs:

Libs|Version
---|---
jquery|~2.1.3
owl-carousel2|~2.0.0-beta.2.4
detectizr|~2.0.0
respond|~1.4.2
picturefill|~2.2.0
fastclick|~1.0.6
foundation|>=5.5.1

## Dev Infos
you can run grunt with:

### build

```bash
grunt build
```

runs task only once (and minify all files)

### dev

```bash
grunt dev
```

This copys all files and that watch's all scss,js,images and fonts for changes, if one changes it automatically starts all task again.

## Misc

### bower
bower packt alle datein in folgenden pfad

```bash
/src/vendor/
```


### Styleguide Path
Bevor man den styleguide nutzen kann muss man einmal grunt dev oder grunt build machen.

Man kann den styleguide auch einfach deaktiveren indem man im gruntfile sassdown auf den task rausnimmt

```bash
/htdocs/site/assets/styleguide/
```

### Processwire NW-Config

* Bevor man die config-nw.php nutzen kann muss man twig replace aktivieren
* die config-nw.php in den site Ordner kopieren und dann in der config.php unten requiren

```bash
/src/docs/config-nw.php
```
