var app = app || {};

app.feedback = (function(window, $) {
	'use strict';
	// use data-module="example" to start this script
	var init = function() {
		console.log('you started example module');
		buildRainbow();
	};
	/**
	 *	Rainbow Adder
	 *	add's .rainbow to body!
	 */
	var buildRainbow = function() {
		[].slice.call( document.querySelectorAll( '.js-feedback' ) ).forEach( function( el ) {
			el.addEventListener( eventtype, function( ev ) {
				classie.add( el, 'trigger' );
				onEndAnimation( el, function() {
					classie.remove( el, 'trigger' );
				} );
			} );
		} );

	};
	return {
		init : init
	};

})(window, jQuery);
