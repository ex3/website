var app = app || {};

app.example = (function(window, $) {
	'use strict';
	// use data-module="example" to start this script
	var init = function() {
		console.log('you started example module');
		buildRainbow();
	};
	/**
	 *	Rainbow Adder
	 *	add's .rainbow to body!
	 */
	var buildRainbow = function() {
		$('body').addClass('rainbow');
	};
	return {
		init : init
	};

})(window, jQuery);
