/**
 * Protect window.console method calls, e.g. console is not defined on IE
 * unless dev tools are open, and IE doesn't define console.debug
 */
(function() {
  if (!window.console) {
    window.console = {};
  }
  // union of Chrome, FF, IE, and Safari console methods
  var m = [
    "log", "info", "warn", "error", "debug", "trace", "dir", "group",
    "groupCollapsed", "groupEnd", "time", "timeEnd", "profile", "profileEnd",
    "dirxml", "assert", "count", "markTimeline", "timeStamp", "clear"
  ];
  // define undefined methods as noops to prevent errors
  for (var i = 0; i < m.length; i++) {
    if (!window.console[m[i]]) {
      window.console[m[i]] = function() {};
    }
  }
})();

/* jQuery Tiny Pub/Sub - v0.7 - 10/27/2011
 * http://benalman.com/
 * Copyright (c) 2011 "Cowboy" Ben Alman; Licensed MIT, GPL */

(function(a) {
    var b = a({});
    a.subscribe = function() {
        b.on.apply(b, arguments);
    }, a.unsubscribe = function() {
        b.off.apply(b, arguments);
    }, a.publish = function() {
        b.trigger.apply(b, arguments);
    };
})(jQuery);

var app = app || {};

app.main = (function(window, $) {

	'use strict';

	var initModules = [];
 	var debugMode	= false;
	/**
	 * Init
	 */
	var init = function init() {

        initLogger();

		if( debugMode ) {
			console.time('module-init');
			console.group('MODULES');
		}
		initSubModules();

	};

    function initLogger() {
        /**
         *  Logt auch im live modus
         */
        app.log = function(message){
            console.log( 'APP: ' + message );
        };
        /**
         *  Logt nur im debug mode
         */
         app.debug = function(message) {
            if( app.main.debugMode ) {
                console.log( '+++ DEBUG: ' + message );
            }
        };
        app.log('init');
        app.debug('init');
    }

	/*
	 * Seitenspezifische Skripte ausführen
	 */
	function initSubModules() {

		var $moduleContainer = $('[data-module]'),
			len,
			i;

		$moduleContainer.each(function() {

			var modules = this.getAttribute('data-module').split(','),
				len = modules.length,
				i = 0;

			if( debugMode ) {
				this.debug(modules);
			}

			for (;i<len;i+=1) {

				if ( initModules.indexOf(modules[i]) > -1 ) {
                    continue;
                }
				if ( !hasOwn(app, modules[i]) ) {
                    continue;
                }
				if ( !hasOwn(app[modules[i]], 'init') || !$.isFunction(app[modules[i]].init) ) {
                    continue;
                }

				initModules.push(modules[i]);
			}

		});

		i = 0;
		len = initModules.length;

		for ( ; i < len; i += 1 ) {
			app[initModules[i]].init();
		}
        if( debugMode ) {
		          console.groupEnd('MODULES');
		      console.timeEnd('module-init');
        }
	}


	/*
	 * HasOwnProperty-helper
	 */
	function hasOwn(obj, prop) {
		return Object.prototype.hasOwnProperty.call(obj, prop);
	}


	return {
		init : init,
		debugMode : debugMode,
		hasOwn : hasOwn
	};



})(window, jQuery);
