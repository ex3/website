'use strict';
module.exports = function (grunt) {
	require('load-grunt-tasks')(grunt, {
		pattern: ['grunt-*', 'sassdown']
	});

	grunt.initConfig({

		pkg: grunt.file.readJSON('package.json'),

		banner: [
			'/*!',
			' * <%= pkg.name %> - Letzter Build: <%= grunt.template.today("yyyy-mm-dd:H:i:s") %>',
			' *',
			' * <%= pkg.description %>',
			' *',
			' * Created by <%= pkg.author %> (<%= pkg.homepage %>)',
			' *',
			' * Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.license %>',
			'**/'
		].join('\n') + '\n',

		paths: {
			src : 			'src/',
			docs : 			'docs/',
			tmpl: 			'htdocs/site/templates/assets/',

			styles: 		'<%= paths.src %>styles/',
			scripts: 		'<%= paths.src %>scripts/',
			icons: 			'<%= paths.src %>icons/',
			images: 		'<%= paths.src %>images/',
			fonts : 		'<%= paths.src %>fonts/',
			vendor: 		'<%= paths.src %>vendor/',
			styleguide: 	'<%= paths.src %>styleguide/',


			tmplStyles: 	'<%= paths.tmpl %>styles/',
			tmplScripts: 	'<%= paths.tmpl %>scripts/',
			tmplIcons: 		'<%= paths.tmpl %>icons/',
			tmplImages: 	'<%= paths.tmpl %>images/',
			tmplFonts: 		'<%= paths.tmpl %>fonts/',
			tmplVendor: 	'<%= paths.tmpl %>vendor/',
			tmplStyleguide: '<%= paths.tmpl %>styleguide/',
		},

		sass: {
			dev: {
				options: {
					precision: 6,
					noCache: false,
					style: 'expanded',
					banner: '<%= banner %>'
				},
				files: {
					'<%= paths.tmplStyles %>main.css': ['<%= paths.styles %>main.scss'],
					'<%= paths.tmplStyles %>fonts.css': ['<%= paths.styles %>fonts.scss']
				}
			},
			production: {
				options: {
					precision: 6,
					noCache: true,
					style: 'compressed',
					banner: '<%= banner %>'
				},
				files: {
					'<%= paths.tmplStyles %>main.min.css': ['<%= paths.styles %>main.scss'],
					'<%= paths.tmplStyles %>fonts.min.css': ['<%= paths.styles %>fonts.scss']
				}
			}
		},
		autoprefixer: {
			options: {
				browsers: ['last 2 versions', 'ie 8', 'ie 9']
			},
			// prefix the specified file
	    	no_dest: {
		    	src: '<%= paths.tmplStyles %>main.css' // globbing is also possible here
		    }
		},
		watch: {
			scss: {
				files: [
					'<%= paths.styles %>/**/*' //check all
				],
				tasks: ['sass:dev','autoprefixer','sassdown:styleguide'],
				options: {
			      livereload: false,
    			}
			},
			fonts :{
				files : [
					'<%= paths.fonts %>/*'
				],
				tasks : ['copy:fonts'],
				options: {
			      livereload: false,
    			}
			},
			images :{
				files : [
					'<%= paths.images %>/*'
				],
				tasks : ['imagemin'],
				options: {
			      livereload: false,
    			}
			},
			vendor :{
				files : [
					'<%= paths.vendor %>/*'
				],
				tasks : ['copy:vendor'],
				options: {
			      livereload: false,
    			}
			},
			scripts : {
				files : [
					'<%= paths.scripts %>/**/*'
				],
				tasks : ['uglify:dev', 'uglify:production', 'copy:libs', 'copy:scripts','sassdown:styleguide'],
				options: {
			      livereload: false,
    			}
			}
		},

		uglify: {
			dev: {
				options: {
					mangle: false,
					beautify: {
						width: 80,
						beautify: true
					},
					compress: false
				},
				files: {
					'<%= paths.tmplScripts %>main.js': [
						'<%= paths.scripts %>libs/**/*.js',
						'<%= paths.scripts %>modules/**/*.js',
						'<%= paths.scripts %>main.js'
					]
				}
			},
			production : {
				options: {
					banner: '<%= banner %>'
				},
				files: {
					'<%= paths.tmplScripts %>main.min.js': [
						'<%= paths.vendor %>picturefill/dist/picturefill.js',
						'<%= paths.vendor %>fastclick/lib/fastclick.js',
						'<%= paths.vendor %>owl-carousel2/dist/owl.carousel.js',
						'<%= paths.scripts %>libs/**/*.js',
						'<%= paths.scripts %>modules/**/*.js',
						'<%= paths.scripts %>main.js'
					]
				}
			}
		},
		imagemin: {
			dynamic: {
				files: [{
					expand: true,
					cwd: '<%= paths.images %>',
					src: ['**/*.{png,jpg,gif,svg}'],
					dest: '<%= paths.tmplImages %>'
				}]
			}
		},
		modernizr : {
			dist: {

				'devFile' : '<%= paths.src %>tmp/modernizr-dev.js',

				// [REQUIRED] Path to save out the built file.
				'outputFile' : '<%= paths.tmplScripts %>libs/modernizr.js',

				// Based on default settings on http://modernizr.com/download/
				'extra' : {
					'shiv' : true,
					'printshiv' : false,
					'load' : true,
					'mq' : false,
					'cssclasses' : true
				},

				// Based on default settings on http://modernizr.com/download/
				'extensibility' : {
					'addtest' : false,
					'prefixed' : false,
					'teststyles' : false,
					'testprops' : false,
					'testallprops' : false,
					'hasevents' : false,
					'prefixes' : false,
					'domprefixes' : false
				},

				// By default, source is uglified before saving
				'uglify' : true,

				// Define any tests you want to implicitly include.
				'tests' : [ 'csstransforms3d', 'backgroundsize', 'cssanimations', 'svg', 'generatedcontent', 'csstransitions'],

				// By default, this task will crawl your project for references to Modernizr tests.
				// Set to false to disable.
				'parseFiles' : false

				// When parseFiles = true, this task will crawl all *.js, *.css, *.scss files, except files that are in node_modules/.
				// You can override this by defining a "files" array below.
				// "files" : {
				// "src": []
				// },

			}
		},

		copy: {
			fonts: {
				files: [{
					expand: true,
					cwd: '<%= paths.fonts %>',
					src: '*.{eot,woff,woff2,ttf,svg}',
					dest: '<%= paths.tmplFonts %>'
				}]
			},
			scripts: {
				files: [{
					expand: true,
					cwd: '<%= paths.scripts %>',
					src: ['**'],
					dest: '<%= paths.tmplScripts %>'
				}]
			},
			libs: {
				files: [{
					expand: true,
					cwd: '<%= paths.scripts %>libs/',
					src: ['**'],
					dest: '<%= paths.tmplScripts %>libs/'
				}]
			},
			images: {
				files: [{
					expand: true,
					cwd: '<%= paths.images %>',
					src: ['**'],
					dest: '<%= paths.tmplImages %>'
				}]
			},
			vendor: {
				files : [{
					expand: true,
					cwd: '<%= paths.vendor %>',
					src: ['**'],
					dest: '<%= paths.tmplVendor %>',
					filter: 'isFile'
				}]
			}
		},

		concurrent: {
			options: {
				limit: 5,
				logConcurrentOutput: true
			},
			dev: {
				tasks: ['watch:scss', 'watch:fonts', 'watch:vendor', 'watch:images', 'watch:scripts']
			}
		},
		sassdown: {
			styleguide: {
				options: {
					assets: [
						'<%= paths.tmplStyles %>*.css',
						'<%= paths.tmplScripts %>*.js'
					],
					template: '<%= paths.styleguide %>styleguide.hbs',
					theme: '<%= paths.styleguide %>styleguide.css',
					readme: '<%= paths.styleguide %>styleguide.md',
            		highlight: 'monokai'
				},

				// alle Dateien des `elements`-Ordners dienen als Styleguide-Module
				files: [{
					expand: true,
					cwd: '<%= paths.styles %>elements/',
					src: ['*.scss'],
					dest: '<%= paths.tmplStyleguide %>'
				}]
			}
		},
	});
	//grunt.file.isDir();
	//grunt.file.mkdir();
	grunt.registerTask('dev', [
		'uglify:production',
		'uglify:dev',

		'sass:dev',

		'modernizr:dist',

		'copy:fonts',
		'copy:vendor',
		'copy:scripts',
		'copy:libs',

		'imagemin',

		'sassdown:styleguide',

		'concurrent:dev'
	]);
	grunt.registerTask('build', [
		'uglify:production',

		'sass:production',

		'modernizr:dist',

		'copy:fonts',
		'copy:vendor',
		'copy:scripts',
		'copy:libs',

		'imagemin',

		'sassdown:styleguide'
	]);

};
